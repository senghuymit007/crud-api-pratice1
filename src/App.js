import "bootstrap/dist/css/bootstrap.min.css";
import Menubar from './components/Menubar'
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Container } from "react-bootstrap";
import 'moment/locale/km';//to change in khmer;
import Category from "./pages/Category";

function App() {
  return (
    <BrowserRouter>
      <Menubar />
      <Container>
        <Switch>
          <Route exact path="/" component={Category} />
        </Switch>
      </Container>
    </BrowserRouter>
  );
}

export default App;
