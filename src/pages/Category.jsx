import React, { useEffect, useState } from 'react';
import { useHistory } from "react-router";
import { Table, Button } from 'react-bootstrap';
import { BrowserRouter as Router } from "react-router-dom";
import { addCategory, deleteCategoryById, fetchAllCategory, fetchCategoryById, updateCategoryById } from '../services/Services';
import { useLocation } from "react-router";
import query from 'query-string'

export default function Category() {

    const [category, setCategory] = useState([]);
    const history = useHistory();
    const [name, setName] = useState("");

    const { search } = useLocation()
    console.log("Location:", search);

    let { id } = query.parse(search)
    console.log("id query:", id);

    //Add/Update category to api
    async function onAddCategory() {
        
        if (search == "") {
            const category = {
                name,
            };
            const result = await addCategory(category);
        } else {
            const category = {
                name,
            };
            
            const result = await updateCategoryById(id, category)
        }
        const result = await fetchAllCategory();
        console.log("Category:", result);
        setCategory(result);
        setName("")
        history.push("")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(async () => {
        const result = await fetchAllCategory();
        console.log("Category:", result);
        setCategory(result);
        if (search == "") {
            setName("")
        } else {
            const result = await fetchCategoryById(id)
            setName(result.name)
        }
    }, [id]);

    //Delete Category by ID
    async function onDeleteCatById(id) {
        const result = await deleteCategoryById(id);
        const temp = category.filter(item => {
            return item._id != id
        })
        setCategory(temp)
    }

    return (
        <div>
            <Router>
                <br />
                <input value={name} onChange={(e) => setName(e.target.value)} type="text" placeholder="Category Name" />
                <Button onClick={onAddCategory} variant="primary">{search ? "Update" : "Add"}</Button>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {category.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <td>{item._id.slice(0, 8)}</td>
                                    <td>{item.name}</td>
                                    <td>
                                        <Button onClick={() => history.push(`/?id=${item._id}`)} variant="warning">Edit</Button>{' '}
                                        <Button onClick={() => onDeleteCatById(item._id)} variant="danger">Delete</Button>
                                    </td>
                                </tr>
                            );
                        })}

                    </tbody>
                </Table>
            </Router>
        </div>
    )
}
